﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_CreateNote : Form
    {
        public Form_CreateNote()
        {
            InitializeComponent();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //If txtbox is not empty
            if (txtNote.Text != string.Empty)
            {
                //Create note-->Add to list-->Add to file
                Notes createdNote = new Notes(txtNote.Text);
                ActiveUser.getInstance().Notelist.Add(createdNote);
                FileOperations.getInstance().WriteModules(createdNote.ToString(), @"path\notes.csv");
                //Go back notebook
                Form notes = new Form_Notes();
                notes.Show();
                this.Hide();
            }
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            //Go back menu
            Form menu = new Form_Menu();
            menu.Show();
            this.Hide();
        }
        private void Form_CreateNote_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
