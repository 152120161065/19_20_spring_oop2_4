﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Crypt
    {
        /// <summary>
        /// Crypt user password to sha256 format
        /// </summary>
        /// <param name="password"> User password </param>
        /// <returns></returns>
        public string Convert_Sha256(string password)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(password));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
        /// <summary>
        /// Convert text to base64 format
        /// </summary>
        /// <param name="plainText"> String to be converted </param>
        /// <returns></returns>
        public string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }
        /// <summary>
        /// Convert base64 text to string
        /// </summary>
        /// <param name="encodedText"> Text which is converted to base64 before </param>
        /// <returns></returns>
        public string Base64Decode(string encodedText)
        {
            var base64EncodedBytes = Convert.FromBase64String(encodedText);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
        /// <summary>
        /// Convert image to base64 format
        /// </summary>
        /// <param name="image"> User specified picture </param>
        /// <returns> Image-base64 format- </returns>
        public string ImageEncode(Image image)
        {
            string base64Img;
            using (MemoryStream m = new MemoryStream())
            {
                image.Save(m, image.RawFormat);
                byte[] imageBytes = m.ToArray();
                base64Img = Convert.ToBase64String(imageBytes);
            }
            return base64Img;
        }
        /// <summary>
        /// Convert base64 string to Image
        /// </summary>
        /// <param name="base64String"> Base64 string which is store in images.csv file </param>
        /// <returns> Image -Converted from base64- </returns>
        public Image ImageDecode(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }
        /// <summary>
        /// Singleton design pattern
        /// </summary>
        private static Crypt instance;
        private Crypt()
        { }
        public static Crypt getInstance()
        {
            if (instance == null)
            {
                instance = new Crypt();
            }
            return instance;
        }
    }
}
