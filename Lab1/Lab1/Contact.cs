﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Contact
    {
        //Definition of properties
        private string name { get; set; }
        private string surname { get; set; }
        private string number { get; set; }
        private string email { get; set; }
        private string description { get; set; }
        private string address { get; set; }
        //Constructor
        public Contact(string name, string surname, string number, string email, string description, string address)
        {
            this.name = name;
            this.surname = surname;
            this.number = number;
            this.email = email;
            this.description = description;
            this.address = address;
        }
        //Getter-Setter methods
        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public string Number { get => number; set => number = value; }
        public string Email { get => email; set => email = value; }
        public string Description { get => description; set => description = value; }
        public string Address { get => address; set => address = value; }
        //override ToString method
        public override string ToString()
        {
            Crypt crypt = Crypt.getInstance();
            return string.Format("{0},{1},{2},{3},{4},{5},{6}", name, surname, number, crypt.Base64Encode(email), crypt.Base64Encode(description), crypt.Base64Encode(address), crypt.Base64Encode(ActiveUser.getInstance().User.Username));
        }
        //Destructor
        ~Contact()
        { }
    }
}
