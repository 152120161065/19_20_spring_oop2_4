﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_SignUp : Form
    {
        public Form_SignUp()
        {
            InitializeComponent();
        }
        private void btnSignUp_Click(object sender, EventArgs e)
        {
            if (this.Controls.OfType<TextBox>().Any(t => string.IsNullOrEmpty(t.Text)))
            {
                lblInfo.Text = "Fill empty fields";
                lblInfo.ForeColor = Color.DarkRed;
                return;
            }
            if (Community.getInstance().Check_CreateUser(txtUsername.Text,txtEmail.Text,txtNumber.Text))
            {
                SignUp_Success();
                return;
            }
            SignUp_Failed();
        }
        private void SignUp_Success()
        {
            //Call instance
            User user;
            Community community = Community.getInstance();
            string cPassword = Crypt.getInstance().Convert_Sha256(txtPassword.Text);
            //Decide access level
            if (community.Userlist.Count == 0)
            {
                user = new User(txtName.Text, txtSurname.Text, txtUsername.Text, cPassword, txtNumber.Text, txtEmail.Text, txtAddress.Text, 0, "Admin", 0, pbxPicture.Image);
            }
            else
            {
                user = new User(txtName.Text, txtSurname.Text, txtUsername.Text, cPassword, txtNumber.Text, txtEmail.Text, txtAddress.Text, 0, "User", 0, pbxPicture.Image);
            }
            //Add Userlist and Save to users.csv...
            community.Userlist.Add(user);
            FileOperations.getInstance().AddUser(ref user);
            //SignUp succesfully
            lblInfo.Text = "Success";
            lblInfo.ForeColor = Color.DarkGreen;
            Enabled = false;
            tmDelay.Start();
        }
        private void NameSurname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
        private void SignUp_Failed()
        {
            //SignUp failed
            lblInfo.Text = "Failed";
            lblInfo.ForeColor = Color.DarkRed;
            return;
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            //Go back ->Login Form
            Form login = new Form_Login();
            login.Show();
            this.Hide();
        }
        private void tmDelay_Tick(object sender, EventArgs e)
        {
            //Time to go -> Login Form
            tmDelay.Stop();
            Form login = new Form_Login();
            login.Show();
            this.Hide();
        }
        private void Form_SignUp_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
        private void btnLoadImg_Click(object sender, EventArgs e)
        {
            if(ofdLoadImg.ShowDialog() == DialogResult.OK)
            {
                pbxPicture.Image = Image.FromFile(ofdLoadImg.FileName);
            }
        }
    }
}
