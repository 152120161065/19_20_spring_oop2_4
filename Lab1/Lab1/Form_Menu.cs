﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_Menu : Form
    {
        public Form_Menu()
        {
            InitializeComponent();
        }
        private void Form_App_Load(object sender, EventArgs e)
        {
            //Logined user is not "Adminstrator" --->
            if (ActiveUser.getInstance().User.Level != "Admin")
            {
                btnManage.Enabled = false;
            }
        }
        private void btnLogOut_Click(object sender, EventArgs e)
        {
            User activeUser = ActiveUser.getInstance().User;
            //Set oldValue for update
            string oldValue = activeUser.ToString();
            //Set status = 0
            activeUser.Status = 0;
            //FileProcess
            FileOperations.getInstance().UpdateMethod(oldValue, activeUser.ToString(), @"path\users.csv");
            //Log out process
            LogOut();
        }
        private void LogOut()
        {
            //Call instance
            ActiveUser activeUser = ActiveUser.getInstance();
            //Clear lists
            activeUser.User = null;
            activeUser.Contactlist.Clear();
            activeUser.Notelist.Clear();
            //Go back ---> Login Form
            Form login = new Form_Login();
            login.Show();
            this.Hide();
        }
        private void btnExport_Click(object sender, EventArgs e)
        {
            //Export all user info's to .tsv file
            if (sfdExport.ShowDialog() == DialogResult.OK)
            {
                FileOperations.getInstance().ExportUsers(sfdExport.FileName);
            }
        }
        private void btnManage_Click(object sender, EventArgs e)
        {
            //Load user management form
            Form management = new Form_UserManagement();
            management.Show();
            this.Hide();
        }
        private void btnPhonebook_Click(object sender, EventArgs e)
        {
            //Load phonebook form
            Form phonebook = new Form_Phonebook();
            phonebook.Show();
            this.Hide();
        }
        private void btnNotes_Click(object sender, EventArgs e)
        {
            //Load notes form
            Form notes = new Form_Notes();
            notes.Show();
            this.Hide();
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            //Set which user's will be edit?
            ActiveUser activeuser = ActiveUser.getInstance();
            activeuser.Selecteduser = activeuser.User;
            //Load Edit Form
            UserTaker taker = UserTaker.getInstance();
            taker.RedoList.Clear();
            taker.UndoList.Clear();
            Form edit = new Form_EditProfile();
            edit.Show(this);
            this.Hide();
        }
        private void btnCalculator_Click(object sender, EventArgs e)
        {
            ActiveUser.getInstance().Selecteduser = ActiveUser.getInstance().User;
            Form calculator = new Form_Calculator();
            calculator.Show();
            this.Hide();
        }
        private void Form_App_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
        private void btnReminder_Click(object sender, EventArgs e)
        {
            Form reminder = new Form_Reminder();
            reminder.Show();
            this.Hide();
        }
    }
}
