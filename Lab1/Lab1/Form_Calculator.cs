﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_Calculator : Form
    {
        double coef = 0.0;
        NumberFormatInfo provider = new NumberFormatInfo();
        Stack<Double> coefs = new Stack<Double>();
        public Form_Calculator()
        {
            InitializeComponent();
            provider.NumberDecimalSeparator = ".";
        }
        private void Calculate(double tag)
        {
            coef += tag;
        }
        private void Calculate(string tag)
        {
            coef += Convert.ToDouble(tag, provider);
        }
        private void CalculateExperience()
        {
            foreach (Control rb in grpExperience.Controls)
            {
                if (rb is RadioButton)
                {
                    if (((RadioButton)rb).Checked)
                    {
                        Calculate(rb.Tag.ToString());
                        break;
                    }
                }
            }
        }
        private void CalculateCity()
        {
            foreach (Control rb in grpCity.Controls)
            {
                if (rb is RadioButton)
                {
                    if (((RadioButton)rb).Checked)
                    {
                        Calculate(rb.Tag.ToString());
                        break;
                    }
                }
            }
        }
        private void CalculateGraduated()
        {
            double maxTag = 0;
            foreach (Control chk in grpGraduated.Controls)
            {
                if (chk is CheckBox)
                {
                    if (((CheckBox)chk).Checked)
                    {
                        if (double.Parse(chk.Tag.ToString()) > maxTag)
                            maxTag = double.Parse(chk.Tag.ToString(), provider);
                    }
                }
            }
            Calculate(maxTag);
        }
        private void CalculateLanguage()
        {
            if (chkLanguage1.Checked || chkLanguage2.Checked)
                Calculate(chkLanguage1.Tag.ToString());
            if (chkLanguage3.Checked)
            {
                for (int i = 0; i < numOtherLanguages.Value; i++)
                {
                    Calculate(chkLanguage3.Tag.ToString());
                }
            }
        }
        private void CalculateTask()
        {
            foreach (Control rb in grpTask.Controls)
            {
                if (rb is RadioButton)
                {
                    if (((RadioButton)rb).Checked)
                    {
                        Calculate(rb.Tag.ToString());
                        break;
                    }
                }
            }
        }
        private void CalculateFamily()
        {
            if (chkFamily1.Checked)
                Calculate(chkFamily1.Tag.ToString());
            if (chkFamily2.Checked)
            {
                for (int i = 0; i < numFamily1.Value; i++)
                {
                    coefs.Push(double.Parse(chkFamily2.Tag.ToString(), provider));
                }
            }
            if (chkFamily3.Checked)
            {
                for (int i = 0; i < numFamily2.Value; i++)
                {
                    coefs.Push(double.Parse(chkFamily3.Tag.ToString(), provider));
                }
            }
            if (chkFamily4.Checked)
            {
                for (int i = 0; i < numFamily3.Value; i++)
                {
                    coefs.Push(double.Parse(chkFamily4.Tag.ToString(), provider));
                }
            }
            for (int i = 0; i < 2; i++)
            {
                if (coefs.Count > 0)
                {
                    Calculate(coefs.Pop());
                }
            }
        }
        private void setSalary()
        {
            User user = ActiveUser.getInstance().Selecteduser;
            string oldValue = user.ToString();
            if(user.Level == "Part-Time")
            {
                user.Salary = (5000 * (coef + 1)) / 2;
            }
            else
            {
                user.Salary = 5000 * (coef + 1);
            }
            FileOperations.getInstance().UpdateMethod(oldValue, user.ToString(), @"Path\users.csv");
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (grpExperience.Visible)
            {
                CalculateExperience();
                grpExperience.Visible = false;
                grpCity.Visible = true;
                lblInfo.Text = "TR42-21: Kocaeli, Sakarya, Düzce, Bolu, Yalova, Edirne, Kırklareli, Tekirdağ";
            }
            else if (grpCity.Visible)
            {
                CalculateCity();
                grpCity.Visible = false;
                grpGraduated.Visible = true;
                lblInfo.Text = String.Empty;
            }
            else if (grpGraduated.Visible)
            {
                CalculateGraduated();
                grpGraduated.Visible = false;
                grpLanguage.Visible = true;
            }
            else if (grpLanguage.Visible)
            {
                CalculateLanguage();
                grpLanguage.Visible = false;
                grpTask.Visible = true;
            }
            else if (grpTask.Visible)
            {
                CalculateTask();
                grpTask.Visible = false;
                grpFamily.Visible = true;
            }
            else if (grpFamily.Visible)
            {
                CalculateFamily();
                grpFamily.Visible = false;
                if (MessageBox.Show("Salary: " + (5000 * (coef + 1)), "Confirm", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    setSalary();
                    Form menu = new Form_Menu();
                    menu.Show();
                    this.Hide();
                }
                else
                {
                    coefs.Clear();
                    coef = 0.0;
                    Controls.Clear();
                    InitializeComponent();
                }
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Form menu = new Form_Menu();
            menu.Show();
            this.Hide();
        }
        private void Form_Calculator_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
