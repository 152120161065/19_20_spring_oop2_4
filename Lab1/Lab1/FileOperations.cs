﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lab1
{
    class FileOperations
    {
        private const string usersPath = @"path\users.csv";
        private const string contactPath = @"path\phonebook.csv";
        private const string notePath = @"path\notes.csv";
        private const string imgPath = @"path\images.csv";
        private const string remindersPath = @"path\reminders.csv";
        /// <summary>
        /// Load users from users.csv
        /// </summary>
        public void LoadUsers()
        {
            if (!Directory.Exists("path"))
            {
                Directory.CreateDirectory("path");
            }
            if (!File.Exists(usersPath))
            {
                FileStream fs = File.Create(usersPath);
                fs.Close();
            }
            if (!File.Exists(contactPath))
            {
                FileStream fs = File.Create(contactPath);
                fs.Close();
            }
            if (!File.Exists(notePath))
            {
                FileStream fs = File.Create(notePath);
                fs.Close();
            }
            if(!File.Exists(imgPath))
            {
                FileStream fs = File.Create(imgPath);
                fs.Close();
            }
            if(!File.Exists(remindersPath))
            {
                FileStream fs = File.Create(remindersPath);
                fs.Close();
            }
            //Call instance
            BindingList<User> userlist = Community.getInstance().Userlist;
            Crypt crypt = Crypt.getInstance();
            //Read users from .csv file
            foreach (var line in File.ReadLines(usersPath))
            {
                string[] parts = line.Split(',');
                parts[2] = crypt.Base64Decode(parts[2]); //Username
                parts[5] = crypt.Base64Decode(parts[5]); //Email
                parts[6] = crypt.Base64Decode(parts[6]); //Address
                User user = new User(parts[0], parts[1],parts[2],parts[3],parts[4],parts[5],parts[6],int.Parse(parts[7]),parts[8],double.Parse(parts[9]));
                userlist.Add(user);
            }
            //Read images from .csv file
            foreach(var line in File.ReadLines(imgPath))
            {
                string[] parts = line.Split(',');
                foreach(User user in userlist)
                {
                    if(crypt.Base64Decode(parts[1]).Equals(user.Username))
                    {
                        user.Picture = crypt.ImageDecode(parts[0]);
                    }
                }
            }
        }
        /// <summary>
        /// Add user to users.csv
        /// </summary>
        public void AddUser(ref User user)
        {
            //User writer
            FileStream fs = new FileStream(usersPath, FileMode.Append, FileAccess.Write);
            StreamWriter signup = new StreamWriter(fs);
            signup.WriteLine(user.ToString());
            //Close stream
            signup.Flush();
            signup.Close();
            fs.Close();
            //Image writer
            FileStream fs2 = new FileStream(imgPath, FileMode.Append, FileAccess.Write);
            StreamWriter addImg = new StreamWriter(fs2);
            addImg.WriteLine(user.ImgToString());
            //Close stream
            addImg.Flush();
            addImg.Close();
            fs2.Close();
        }
        /// <summary>
        /// Update modules according to given paramaters
        /// </summary
        public void UpdateMethod(string oldValue, string newValue, string filePath)
        {
            string text = File.ReadAllText(filePath);
            text = text.Replace(oldValue, newValue);
            File.WriteAllText(filePath, text);
        }
        /// <summary>
        /// Delete modules according to given parameters
        /// </summary>
        public void DeleteMethod(string oldValue, string filePath)
        {
            var tempFile = Path.GetTempFileName();
            var linesToKeep = File.ReadLines(filePath).Where(l => l != oldValue);
            File.WriteAllLines(tempFile, linesToKeep);
            File.Delete(filePath);
            File.Move(tempFile, filePath);
        }
        /// <summary>
        /// Export users to filePath (see Form_App.btnExport_Click())
        /// </summary>
        public void ExportUsers(string filePath)
        {
            StreamWriter export = new StreamWriter(filePath);
            BindingList<User> userlist = Community.getInstance().Userlist;
            foreach (User value in userlist)
            {
                export.WriteLine("{0}\t{1}\t{2}", value.Username, value.Password, value.Status);
            }
            export.Flush();
            export.Close();
        }
        /// <summary>
        /// Load modules from their files (phonebook.csv , notes. csv etc.)
        /// </summary>
        public void LoadModules()
        {
            //Call instance
            ActiveUser active = ActiveUser.getInstance();
            BindingList<Contact> contactlist = active.Contactlist;
            BindingList<Notes> notelist = active.Notelist;
            BindingList<Reminder> reminderlist = active.Reminderlist;
            User user = active.User;
            Crypt crypt = Crypt.getInstance();
            //Read--> Split --> Decode if need --> Call constructor --> Add to contactlist
            foreach (var line in File.ReadLines(contactPath))
            {
                string[] parts = line.Split(',');
                parts[3] = crypt.Base64Decode(parts[3]); //Email
                parts[4] = crypt.Base64Decode(parts[4]); //Description
                parts[5] = crypt.Base64Decode(parts[5]); //Address
                parts[6] = crypt.Base64Decode(parts[6]); //ActiveUser's username
                if (parts[6] == user.Username)
                {
                    Contact contact = new Contact(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5]);
                    contactlist.Add(contact);
                }
            }
            //Read --> Split --> Decode if need--> Call constructor --> Add to notelist
            foreach (var line in File.ReadLines(notePath))
            {
                string[] parts = line.Split(',');
                parts[0] = crypt.Base64Decode(parts[0]); //Note
                parts[1] = crypt.Base64Decode(parts[1]); //ActiveUser's username
                if (parts[1] == user.Username)
                {
                    Notes note = new Notes(parts[0]);
                    notelist.Add(note);
                }
            }
            //Read --> Split --> Decode --> Constructor --> Add to reminderList
            foreach(var line in File.ReadLines(remindersPath))
            {
                string[] parts = line.Split(',');
                parts[2] = crypt.Base64Decode(parts[2]); //Summary
                parts[3] = crypt.Base64Decode(parts[3]); //Description
                parts[4] = crypt.Base64Decode(parts[4]); //Username
                if (parts[4] == user.Username)
                {
                    Reminder r = new Reminder(parts[0], DateTime.Parse(parts[1]), parts[2], parts[3]);
                    reminderlist.Add(r);
                }
            }
        }
        /// <summary>
        /// Write module to .csv file
        /// </summary>
        /// <param name="plaintext"> The format to be written to .csv file (see toString() methods in classes) </param>
        /// <param name="filePath"> Which file to write </param>
        public void WriteModules(string plaintext, string filePath)
        {
            FileStream fs = new FileStream(filePath, FileMode.Append, FileAccess.Write);
            StreamWriter add = new StreamWriter(fs);
            add.WriteLine(plaintext);
            //Close stream;
            add.Flush();
            add.Close();
            fs.Close();
        }
        /// <summary>
        /// Singleton Design Pattern
        /// </summary>
        private static FileOperations instance;
        private FileOperations()
        { }
        public static FileOperations getInstance()
        {
            if (instance == null)
            {
                instance = new FileOperations();
            }
            return instance;
        }
    }
}
