﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_CreateReminder : Form
    {
        public Form_CreateReminder()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!rbMeeting.Checked && !rbTask.Checked)
            {
                return;
            }
            else
            {
                Reminder r;
                if (rbMeeting.Checked)
                {
                    r = new Reminder("Meeting", dtpDate.Value, txtSummary.Text, txtDescription.Text);
                }
                else
                {
                    r = new Reminder("Task", dtpDate.Value, txtSummary.Text, txtDescription.Text);
                }
                FileOperations.getInstance().WriteModules(r.ToString(), @"path\reminders.csv");
                ActiveUser.getInstance().Reminderlist.Add(r);
                //Go Back ReminderList
                Form reminder = new Form_Reminder();
                reminder.Show();
                this.Hide();
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Form reminder = new Form_Reminder();
            reminder.Show();
            this.Hide();
        }
        private void Form_CreateReminder_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
