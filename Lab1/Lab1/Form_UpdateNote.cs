﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_UpdateNote : Form
    {
        public Form_UpdateNote()
        {
            InitializeComponent();
        }
        private void Form_UpdateNote_Load(object sender, EventArgs e)
        {
            //Set txtbox with selected note
            txtNote.Text = ActiveUser.getInstance().Selectednote.Note;
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            Notes updateNote = ActiveUser.getInstance().Selectednote;
            if (txtNote.Text != string.Empty)
            {
                //Set oldValue for update method
                string oldValue = updateNote.ToString();
                //Update user note
                updateNote.Note = txtNote.Text;
                //Process on to file...
                FileOperations.getInstance().UpdateMethod(oldValue, updateNote.ToString(), @"path\notes.csv");
                //Go back note form
                Form notes = new Form_Notes();
                notes.Show();
                this.Hide();
            }
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            //Go back note form
            Form notes = new Form_Notes();
            notes.Show();
            this.Hide();
        }
        private void Form_UpdateNote_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
