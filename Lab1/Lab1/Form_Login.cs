﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_Login : Form
    {
        public Form_Login()
        {
            InitializeComponent();
        }
        private void Form_Login_Load(object sender, EventArgs e)
        {
            //Check remember me status
            if (Community.getInstance().Check_RememberMe())
            {
                //Call instance for active user
                User activeUser = ActiveUser.getInstance().User;
                //Set txtboxes with remembered user
                txtUsername.Text = activeUser.Username;
                txtPassword.Text = activeUser.Password;
                //Login succesfully
                Login_Success();
            }
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            //Is there any empty field?
            if (this.Controls.OfType<TextBox>().Any(t => string.IsNullOrEmpty(t.Text)))
            {
                lblInfo.Text = "Fill empty fields";
                lblInfo.ForeColor = Color.DarkRed;
                return;
            }
            //Username&Password Control
            if (Community.getInstance().Check_Login(txtUsername.Text, txtPassword.Text))
            {
                //Correct username and password--->
                Login_Success();
                return;
            }
            //Wrong username or password--->
            Login_Failed();
        }
        private void Login_Success()
        {
            //Check RememberMe checkbox status
            if (chkRemember.Checked)
            {
                //Call instance
                User activeUser = ActiveUser.getInstance().User;
                //Set oldValue for update status
                string oldValue = activeUser.ToString();
                //Set status = 1
                activeUser.Status = 1;
                //FileProcess
                FileOperations.getInstance().UpdateMethod(oldValue, activeUser.ToString(), @"path\users.csv");
            }
            //Login succesfully
            lblInfo.Text = "Success";
            lblInfo.ForeColor = Color.DarkGreen;
            FileOperations.getInstance().LoadModules();
            ActiveUser.getInstance().ReminderController = ReminderController.getInstance();
            ReminderController.getInstance().ReminderList = ActiveUser.getInstance().Reminderlist;
            ActiveUser.getInstance().ReminderController.Run();
            tmDelay.Start();
            Enabled = false;
        }
        private void Login_Failed()
        {
            //Login failed
            lblInfo.Text = "Failed";
            lblInfo.ForeColor = Color.DarkRed;
        }
        private void lnkSignUp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //SignUp form is coming...
            Form signup = new Form_SignUp();
            signup.Show();
            this.Hide();
        }
        private void tmDelay_Tick(object sender, EventArgs e)
        {
            //End of the time...
            tmDelay.Stop();
            Form menu = new Form_Menu();
            menu.Show();
            this.Hide();
        }
        private void Form_Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
