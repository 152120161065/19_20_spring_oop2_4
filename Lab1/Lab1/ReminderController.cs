﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    class ReminderController
    {
        Timer timer;
        private BindingList<Reminder> reminderList { get; set; }
        //Getter-Setter
        public BindingList<Reminder> ReminderList { get => reminderList; set => reminderList = value; }
        public void Run()
        {
            timer.Start();
        }
        public void Stop()
        {
            timer.Stop();
        }
        private void Shake()
        {
            foreach (Form frm in Application.OpenForms)
            {
                if (frm.Visible)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        frm.Left += 10;
                        System.Threading.Thread.Sleep(50);
                        frm.Left -= 10;
                        System.Threading.Thread.Sleep(50);
                    }
                }
            }
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (reminderList != null)
            {
                foreach (Reminder r in reminderList)
                {
                    if (r.Date.ToString().Equals(DateTime.Now.ToString()))
                    {
                        Shake();
                        MessageBox.Show(r.Summary, "Reminder!!!");
                        FileOperations.getInstance().DeleteMethod(r.ToString(), @"path\reminders.csv");
                        reminderList.Remove(r);
                        break;
                    }
                }
            }
        }
        /// <summary>
        /// Singleton Design Pattern
        /// </summary>
        private static ReminderController instance;
        private ReminderController()
        {
            timer = new Timer();
            timer.Interval = 1000;
            timer.Tick += Timer_Tick;
        }
        public static ReminderController getInstance()
        {
            if (instance == null)
            {
                instance = new ReminderController();
            }
            return instance;
        }
    }
}
