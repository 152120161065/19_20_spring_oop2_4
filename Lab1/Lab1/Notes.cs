﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Notes
    {
        //Definition of property
        private string note { get; set; }
        //Constructor
        public Notes(string note)
        {
            this.note = note;
        }
        //Getter-Setter method
        public string Note { get => note; set => note = value; }
        /// <summary>
        /// Override to string method for set desired string format
        /// </summary>
        /// <returns> Returns desired string format </returns>
        public override string ToString()
        {
            Crypt crypt = Crypt.getInstance();
            return string.Format("{0},{1}", crypt.Base64Encode(note), crypt.Base64Encode(ActiveUser.getInstance().User.Username));
        }
    }
}
