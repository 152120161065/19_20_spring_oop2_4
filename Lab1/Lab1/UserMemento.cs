﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class UserMemento
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Password { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public Image Picture { get; set; }
        public UserMemento(string name, string surname, string number, string email, string address, Image picture)
        {
            Name = name;
            Surname = surname;
            Number = number;
            Email = email;
            Address = address;
            Picture = picture;
        }
    }
}
