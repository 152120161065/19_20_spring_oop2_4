﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class UserTaker
    {
        Stack<UserMemento> undoList { get; }
        Stack<UserMemento> redoList { get; }
        public Stack<UserMemento> UndoList { get => undoList; }
        public Stack<UserMemento> RedoList { get => redoList; }
        /// <summary>
        /// Singleton Design Pattern
        /// </summary>
        private static UserTaker instance;
        private UserTaker()
        {
            undoList = new Stack<UserMemento>();
            redoList = new Stack<UserMemento>();
        }
        public static UserTaker getInstance()
        {
            if (instance == null)
            {
                instance = new UserTaker();
            }
            return instance;
        }
    }
}
