﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_UpdateReminder : Form
    {
        Reminder r = ActiveUser.getInstance().Selectedreminder;
        public Form_UpdateReminder()
        {
            InitializeComponent();
        }
        private void Form_UpdateReminder_Load(object sender, EventArgs e)
        {
            if (r.Type == "Task")
            {
                rbTask.Checked = true;
            }
            else
            {
                rbMeeting.Checked = true;
            }
            dtpDate.Value = r.Date;
            txtSummary.Text = r.Summary;
            txtDescription.Text = r.Description;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            string oldValue = r.ToString();
            if (rbMeeting.Checked)
            {
                r.Type = rbMeeting.Text;
            }
            else
            {
                r.Type = rbTask.Text;
            }
            r.Date = dtpDate.Value;
            r.Summary = txtSummary.Text;
            r.Description = txtDescription.Text;
            FileOperations.getInstance().UpdateMethod(oldValue, r.ToString(), @"path\reminders.csv");
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Form reminders = new Form_Reminder();
            reminders.Show();
            this.Hide();
        }
        private void Form_UpdateReminder_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
