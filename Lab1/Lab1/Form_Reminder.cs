﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_Reminder : Form
    {
        public Form_Reminder()
        {
            InitializeComponent();
        }
        private void Form_Reminder_Load(object sender, EventArgs e)
        {
            dgvReminders.DataSource = ActiveUser.getInstance().Reminderlist;
            if (dgvReminders.Rows.Count != 0)
            {
                btnDelete.Enabled = true;
                btnUpdate.Enabled = true;
            }
        }
        private void btnCreate_Click(object sender, EventArgs e)
        {
            Form create = new Form_CreateReminder();
            create.Show();
            this.Hide();
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ActiveUser.getInstance().Selectedreminder = dgvReminders.CurrentRow.DataBoundItem as Reminder;
            //Load Update Form
            Form update = new Form_UpdateReminder();
            update.Show();
            this.Hide();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure?", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Reminder r = dgvReminders.CurrentRow.DataBoundItem as Reminder;
                //Delete from list and file
                FileOperations.getInstance().DeleteMethod(r.ToString(), @"path\reminders.csv");
                ActiveUser.getInstance().Reminderlist.Remove(r);
            }
            //Don't have reminder? We don't need these buttons
            if (dgvReminders.Rows.Count == 0)
            {
                btnDelete.Enabled = false;
                btnUpdate.Enabled = false;
            }
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            Form menu = new Form_Menu();
            menu.Show();
            this.Hide();
        }
        private void Form_Reminder_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
