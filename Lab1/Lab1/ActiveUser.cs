﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lab1
{
    class ActiveUser
    {
        //Definition of properties
        Regex MailFormat;
        private User user { get; set; }
        private BindingList<Contact> contactlist { get; set; }
        private Contact selectedcontact { get; set; }
        private BindingList<Notes> notelist { get; set; }
        private Notes selectednote { get; set; }
        private User selecteduser { get; set; }
        private BindingList<Reminder> reminderlist { get; set; }
        private Reminder selectedreminder { get; set; }
        private ReminderController reminderController { get; set; }
        //Getter-Setter Methods
        public User User { get => user; set => user = value; }
        public BindingList<Contact> Contactlist { get => contactlist; set => contactlist = value; }
        public Contact Selectedcontact { get => selectedcontact; set => selectedcontact = value; }
        public BindingList<Notes> Notelist { get => notelist; set => notelist = value; }
        public Notes Selectednote { get => selectednote; set => selectednote = value; }
        public User Selecteduser { get => selecteduser; set => selecteduser = value; }
        public BindingList<Reminder> Reminderlist { get => reminderlist; set => reminderlist = value; }
        public Reminder Selectedreminder { get => selectedreminder; set => selectedreminder = value; }
        public ReminderController ReminderController { get => reminderController; set => reminderController = value; }
        /// <summary>
        /// Check availability for new contact according to given paramaters
        /// </summary>
        /// <param name="email"> User specified email </param>
        /// <param name="number"> User specified number </param>
        /// <returns> Return availability result </returns>
        public bool Check_CreateContact(string email, string number)
        {
            if (MailFormat.IsMatch(email))
            {
                foreach (Contact contact in contactlist)
                {
                    if (contact.Email.Equals(email) || contact.Number.Equals(number))
                        return false;
                }
                return true;
            }
            return false;
        }
        /// <summary>
        /// Check availability for update contact
        /// </summary>
        /// <param name="email"> User specified email </param>
        /// <param name="number"> User specified number </param>
        /// <returns> Returns availability result </returns>
        public bool Check_UpdateContact(string email, string number)
        {
            if (MailFormat.IsMatch(email))
            {
                foreach (Contact contact in contactlist)
                {
                    if (!contact.Equals(selectedcontact))
                    {
                        if (contact.Email.Equals(email) || contact.Number.Equals(number))
                            return false;
                    }
                }
                return true;
            }
            return false;
        }
        public bool Check_UpdateUser(string email,string number)
        {
            BindingList<User> userlist = Community.getInstance().Userlist;
            if(MailFormat.IsMatch(email))
            {
                foreach(User user in userlist)
                {
                    if(!user.Equals(selecteduser))
                    {
                        if (user.Email.Equals(email) || user.Number.Equals(number))
                            return false;
                    }
                }
                return true;
            }
            return false;
        }
        /// <summary>
        /// Singleton Design Pattern
        /// </summary>
        private static ActiveUser instance;
        private ActiveUser()
        {
            MailFormat = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
            contactlist = new BindingList<Contact>();
            notelist = new BindingList<Notes>();
            reminderlist = new BindingList<Reminder>();
        }
        public static ActiveUser getInstance()
        {
            if (instance == null)
            {
                instance = new ActiveUser();
            }
            return instance;
        }
    }
}
