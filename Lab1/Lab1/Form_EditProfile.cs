﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_EditProfile : Form
    {
        User selectedUser = ActiveUser.getInstance().Selecteduser;
        bool password_changed = false;
        UserTaker taker = UserTaker.getInstance();
        private string storeName;
        private string storeSurname;
        private string storeNumber;
        private string storeEmail;
        private string storeAddress;
        private Image storePicture;
        public Form_EditProfile()
        {
            InitializeComponent();
        }
        private void Form_EditProfile_Load(object sender, EventArgs e)
        {
            lblWelcome.Text = selectedUser.Username + "'s PROFILE";
            Fill_UserInfos();  //Set txtboxes and pbx with user infos
            StoreCurrentInfo(); //Store initial values
        }
        private void StoreCurrentInfo()
        {
            //Store for undo inital values
            storeName = txtName.Text;
            storeSurname = txtSurname.Text;
            storeNumber = txtNumber.Text;
            storeEmail = txtEmail.Text;
            storeAddress = txtAddress.Text;
            storePicture = pbxPicture.Image;
        }
        private void Fill_UserInfos()
        {
            //Fill txtboxes user info's
            txtName.Text = selectedUser.Name;
            txtSurname.Text = selectedUser.Surname;
            txtPassword.Text = selectedUser.Password;
            txtNumber.Text = selectedUser.Number;
            txtEmail.Text = selectedUser.Email;
            txtAddress.Text = selectedUser.Address;
            lblSalary.Text += selectedUser.Salary.ToString();
            pbxPicture.Image = selectedUser.Picture;
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            if (this.Controls.OfType<TextBox>().Any(t => string.IsNullOrEmpty(t.Text)))
            {
                lblInfo.Text = "Fill empty fields";
                lblInfo.ForeColor = Color.DarkRed;
                return;
            }
            if (ActiveUser.getInstance().Check_UpdateUser(txtEmail.Text, txtNumber.Text))
            {
                Update_Success();
                return;
            }
            Update_Failed();
        }
        private void btnLoadImg_Click(object sender, EventArgs e)
        {
            if (ofdLoadImg.ShowDialog() == DialogResult.OK)
            {
                pbxPicture.Image = Image.FromFile(ofdLoadImg.FileName);
            }
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            Owner.Show();
            Hide();
        }
        private void NameSurname_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Block non-alphabetic char
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
        private void txtPassword_Click(object sender, EventArgs e)
        {
            //Control for password was changed?
            txtPassword.Text = string.Empty;
            password_changed = true;
        }
        private void Update_Success()
        {
            User selectedUser = ActiveUser.getInstance().Selecteduser;
            //Set oldValue for update method
            string oldValue = selectedUser.ToString();
            string oldValue_Img = selectedUser.ImgToString();
            //Set new infos for user
            selectedUser.Name = txtName.Text;
            selectedUser.Surname = txtSurname.Text;
            selectedUser.Number = txtNumber.Text;
            selectedUser.Email = txtEmail.Text;
            selectedUser.Address = txtAddress.Text;
            if (password_changed)
            {
                selectedUser.Password = Crypt.getInstance().Convert_Sha256(txtPassword.Text);
            }
            selectedUser.Picture = pbxPicture.Image;
            //Call instance once
            FileOperations fo = FileOperations.getInstance();
            fo.UpdateMethod(oldValue, selectedUser.ToString(), @"path\users.csv");
            fo.UpdateMethod(oldValue_Img, selectedUser.ImgToString(), @"path\images.csv");
            //Go to back...
            Owner.Show();
            Hide();
        }
        private void Update_Failed()
        {
            lblInfo.Text = "Existing infos or invalid email!";
            lblInfo.ForeColor = Color.DarkRed;
        }
        private void Form_EditProfile_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.Z)
            {
                //If UndoList available
                if (taker.UndoList.Count >= 1)
                {
                    UserMemento newMemento = new UserMemento(txtName.Text, txtSurname.Text, txtNumber.Text, txtEmail.Text, txtAddress.Text, pbxPicture.Image);
                    taker.RedoList.Push(newMemento); //Push redolist current values for redo
                    //Undo
                    UserMemento m = taker.UndoList.Pop();
                    txtName.Text = m.Name;
                    txtSurname.Text = m.Surname;
                    txtNumber.Text = m.Number;
                    txtEmail.Text = m.Email;
                    txtAddress.Text = m.Address;
                    pbxPicture.Image = m.Picture;
                    StoreCurrentInfo();
                }
            }
            if (e.Control && e.KeyCode == Keys.Y)
            {
                //If RedoList available
                if (taker.RedoList.Count >= 1)
                {
                    UserMemento newMemento = new UserMemento(txtName.Text, txtSurname.Text, txtNumber.Text, txtEmail.Text, txtAddress.Text, pbxPicture.Image);
                    taker.UndoList.Push(newMemento); //Push undolist current values for undo
                    //Redo
                    UserMemento m = taker.RedoList.Pop();
                    txtName.Text = m.Name;
                    txtSurname.Text = m.Surname;
                    txtNumber.Text = m.Number;
                    txtEmail.Text = m.Email;
                    txtAddress.Text = m.Address;
                    pbxPicture.Image = m.Picture;
                    StoreCurrentInfo();
                }
            }
        }
        private void SaveMemento(object sender, EventArgs e)
        {
            //Save current values when user (leave-event) from textbox
            UserMemento newMemento = new UserMemento(storeName, storeSurname, storeNumber, storeEmail, storeAddress, storePicture);
            taker.UndoList.Push(newMemento);
            StoreCurrentInfo();
        }
        private void btnUpdateSalary_Click(object sender, EventArgs e)
        {
            Form calculator = new Form_Calculator();
            calculator.Show();
            this.Hide();
        }
        void ShowProgressBar()
        {
            pbarSend.Visible = true;
            pbarSend.Enabled = true;
            Enabled = false;
        }
        void StopProgressBar()
        {
            pbarSend.Visible = false;
            pbarSend.Enabled = false;
            Enabled = true;
        }
        private void btnSend_Click(object sender, EventArgs e)
        {
            ShowProgressBar();
            BackgroundWorker bg = new BackgroundWorker();
            bg.DoWork += Bg_DoWork;
            bg.RunWorkerCompleted += Bg_RunWorkerCompleted;
            bg.RunWorkerAsync();
        }
        private void Bg_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            StopProgressBar();
        }
        private void Bg_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                //Definition
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                //Setting for mail
                mail.From = new MailAddress("19.20.oop2.4@gmail.com");
                mail.To.Add(ActiveUser.getInstance().Selecteduser.Email);
                mail.Subject = "Reset Password";
                //Set new password for user
                int newPassword = new Random().Next(1000, 9999);
                SetPassword(newPassword);
                //Set Mail body
                mail.Body = "Your Password: " + newPassword;
                //Setting for smtp
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("19.20.oop2.4", "19_20_oop2_4");
                SmtpServer.EnableSsl = true;
                //Send
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void SetPassword(int newPassword)
        {
            string oldValue = ActiveUser.getInstance().Selecteduser.ToString();
            ActiveUser.getInstance().Selecteduser.Password = Crypt.getInstance().Convert_Sha256(newPassword.ToString());
            FileOperations.getInstance().UpdateMethod(oldValue, ActiveUser.getInstance().Selecteduser.ToString(), @"path\users.csv");
        }
        private void Form_EditProfile_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
