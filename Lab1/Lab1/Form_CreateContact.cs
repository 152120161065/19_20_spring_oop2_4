﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_CreateContact : Form
    {
        public Form_CreateContact()
        {
            InitializeComponent();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //Check is there any empty field
            if (this.Controls.OfType<TextBox>().Any(t => string.IsNullOrEmpty(t.Text)))
            {
                lblInfo.Text = "Fill empty fields";
                lblInfo.ForeColor = Color.DarkRed;
                return;
            }
            //Check availability for create contact with these infos
            if (ActiveUser.getInstance().Check_CreateContact(txtEmail.Text, txtNumber.Text))
            {
                //Sucess
                Create_Success();
                return;
            }
            //Failed
            Create_Failed();
        }
        private void Create_Success()
        {
            //Call instance
            ActiveUser check = ActiveUser.getInstance();
            //Create contact
            Contact contact = new Contact(txtName.Text, txtSurname.Text, txtNumber.Text, txtEmail.Text, txtDescription.Text, txtAddress.Text);
            //Save to list and file
            check.Contactlist.Add(contact);
            FileOperations.getInstance().WriteModules(contact.ToString(), @"path\phonebook.csv");
            //Go back phonebook
            Form phonebook = new Form_Phonebook();
            phonebook.Show();
            this.Hide();
        }
        private void Create_Failed()
        {
            //Contact create process failed
            lblInfo.Text = "Contact already exist or invalid email";
            lblInfo.ForeColor = Color.DarkRed;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            //Go back phonebook
            Form phonebook = new Form_Phonebook();
            phonebook.Show();
            this.Hide();
        }
        private void NameSurname_KeyPress(object sender, KeyPressEventArgs e)
        {
            //User can use only alphabetical char
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
        private void Form_CreateContact_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
