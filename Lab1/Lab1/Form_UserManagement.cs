﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_UserManagement : Form
    {
        public Form_UserManagement()
        {
            InitializeComponent();
        }
        private void Form_UserManagement_Load(object sender, EventArgs e)
        {
            //Set data source for data grid view
            dgvUserList.DataSource = Community.getInstance().Userlist;
            //Set combobox inital value to show
            cmbLevel.Text = cmbLevel.Items[0].ToString();
        }
        private void dgvUserList_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //Which properties will show to user?
            dgvUserList.Columns["Name"].Visible = false;
            dgvUserList.Columns["Surname"].Visible = false;
            dgvUserList.Columns["Password"].Visible = false;
            dgvUserList.Columns["Number"].Visible = false;
            dgvUserList.Columns["Email"].Visible = false;
            dgvUserList.Columns["Address"].Visible = false;
            dgvUserList.Columns["Picture"].Visible = false;
            dgvUserList.Columns["Status"].Visible = false;
            dgvUserList.Columns["Salary"].Visible = false;
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            User user = dgvUserList.CurrentRow.DataBoundItem as User;
            if (user.Equals(Community.getInstance().Userlist[0]))
            {
                MessageBox.Show("Cannot change this user permission's", "AccessError", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                if (!user.Level.Equals(cmbLevel.Text))
                {
                    //Set oldValue for update method
                    string oldValue = user.ToString();
                    //Set new level of user
                    user.Level = cmbLevel.Text;
                    //Set salary due to part-time user
                    if(user.Level == "Part-Time")
                    {
                        user.Salary = (user.Salary) / 2;
                    }
                    //Process FileOperations
                    FileOperations.getInstance().UpdateMethod(oldValue, user.ToString(), @"path\users.csv");
                    //If logined user isn't "Adminstrator" anymore
                    if (ActiveUser.getInstance().User.Level != "Admin")
                    {
                        Form menu = new Form_Menu();
                        menu.Show();
                        this.Hide();
                    }
                    dgvUserList.Refresh();
                    return;
                }
            }
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            ActiveUser.getInstance().Selecteduser = dgvUserList.CurrentRow.DataBoundItem as User;
            //Load Edit Form
            UserTaker taker = UserTaker.getInstance();
            taker.RedoList.Clear();
            taker.UndoList.Clear();
            Form edit = new Form_EditProfile();
            edit.Show(this);
            this.Hide();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            //Go back menu
            Form menu = new Form_Menu();
            menu.Show();
            this.Hide();
        }
        private void Form_UserManagement_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
