﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class User
    {
        //Definition of properties
        private string name { get; set; } //V1
        private string surname { get; set; } //V2
        private string username { get; set; } //V3
        private string password { get; set; } //V4
        private string number { get; set; } //V5
        private string email { get; set; } //V6
        private string address { get; set; } //V7
        private int status { get; set; } //V8
        private string level { get; set; } //V9
        private double salary { get; set; } //V10
        private Image picture { get; set; } //V11
        //Constructor(s)
        public User(string v1,string v2,string v3,string v4,string v5,string v6,string v7,int v8,string v9,double v10 = 0, Image v11 = null)
        {
            name = v1;
            surname = v2;
            username = v3;
            password = v4;
            number = v5;
            email = v6;
            address = v7;
            status = v8;
            level = v9;
            salary = v10;
            picture = v11;
        }
        //Getter-Setter methods
        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Number { get => number; set => number = value; }
        public string Email { get => email; set => email = value; }
        public string Address { get => address; set => address = value; }
        public int Status { get => status; set => status = value; }
        public string Level { get => level; set => level = value; }
        public double Salary { get => salary; set => salary = value; }
        public Image Picture { get => picture; set => picture = value; }
        /// <summary>
        /// Override ToString method for desired string format
        /// </summary>
        /// <returns> Returns desired string format </returns>
        public override string ToString()
        {
            Crypt crypt = Crypt.getInstance();
            string cV3 = crypt.Base64Encode(username);
            string cV6 = crypt.Base64Encode(email);
            string cV7 = crypt.Base64Encode(address);
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", name, surname, cV3, password, number, cV6, cV7, status, level, salary);
        }
        public string ImgToString()
        {
            Crypt crypt = Crypt.getInstance();
            return string.Format("{0},{1}", crypt.ImageEncode(picture), crypt.Base64Encode(username));
        }
        //Destructor
        ~User()
        { }
    }
}
