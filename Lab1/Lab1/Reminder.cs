﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Reminder
    {
        //Definiton of properties
        private string type { get; set; }
        private DateTime date { get; set; }
        private string summary { get; set; }
        private string description { get; set; }
        //Constructor
        public Reminder(string type, DateTime date, string summary, string description)
        {
            this.type = type;
            this.date = date;
            this.summary = summary;
            this.description = description;
        }
        //Getter-Setter
        public string Type { get => type; set => type = value; }
        public DateTime Date { get => date; set => date = value; }
        public string Summary { get => summary; set => summary = value; }
        public string Description { get => description; set => description = value; }
        //Override ToString
        public override string ToString()
        {
            Crypt crypt = Crypt.getInstance();
            string cSummary = crypt.Base64Encode(summary);
            string cDescription = crypt.Base64Encode(description);
            string cUsername = crypt.Base64Encode(ActiveUser.getInstance().User.Username);
            return string.Format("{0},{1},{2},{3},{4}", type, date.ToString(), cSummary, cDescription, cUsername);
        }
        //Destructor
        ~Reminder()
        { }
    }
}
