﻿namespace Lab1
{
    partial class Form_Calculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkLanguage1 = new System.Windows.Forms.CheckBox();
            this.chkFamily1 = new System.Windows.Forms.CheckBox();
            this.grpTask = new System.Windows.Forms.GroupBox();
            this.rbTask6 = new System.Windows.Forms.RadioButton();
            this.rbTask5 = new System.Windows.Forms.RadioButton();
            this.rbTask4 = new System.Windows.Forms.RadioButton();
            this.rbTask3 = new System.Windows.Forms.RadioButton();
            this.rbTask2 = new System.Windows.Forms.RadioButton();
            this.rbTask1 = new System.Windows.Forms.RadioButton();
            this.grpGraduated = new System.Windows.Forms.GroupBox();
            this.chkGraduated5 = new System.Windows.Forms.CheckBox();
            this.chkGraduated3 = new System.Windows.Forms.CheckBox();
            this.chkGraduated4 = new System.Windows.Forms.CheckBox();
            this.chkGraduated1 = new System.Windows.Forms.CheckBox();
            this.chkGraduated2 = new System.Windows.Forms.CheckBox();
            this.chkFamily4 = new System.Windows.Forms.CheckBox();
            this.chkFamily3 = new System.Windows.Forms.CheckBox();
            this.numFamily3 = new System.Windows.Forms.NumericUpDown();
            this.numFamily2 = new System.Windows.Forms.NumericUpDown();
            this.numFamily1 = new System.Windows.Forms.NumericUpDown();
            this.chkFamily2 = new System.Windows.Forms.CheckBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.grpFamily = new System.Windows.Forms.GroupBox();
            this.grpExperience = new System.Windows.Forms.GroupBox();
            this.rbExperience6 = new System.Windows.Forms.RadioButton();
            this.rbExperience5 = new System.Windows.Forms.RadioButton();
            this.rbExperience4 = new System.Windows.Forms.RadioButton();
            this.rbExperience3 = new System.Windows.Forms.RadioButton();
            this.rbExperience2 = new System.Windows.Forms.RadioButton();
            this.rbExperience1 = new System.Windows.Forms.RadioButton();
            this.grpCity = new System.Windows.Forms.GroupBox();
            this.rbCity1 = new System.Windows.Forms.RadioButton();
            this.rbCity2 = new System.Windows.Forms.RadioButton();
            this.rbCity3 = new System.Windows.Forms.RadioButton();
            this.rbCity4 = new System.Windows.Forms.RadioButton();
            this.chkLanguage2 = new System.Windows.Forms.CheckBox();
            this.grpLanguage = new System.Windows.Forms.GroupBox();
            this.numOtherLanguages = new System.Windows.Forms.NumericUpDown();
            this.chkLanguage3 = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblInfo = new System.Windows.Forms.Label();
            this.grpTask.SuspendLayout();
            this.grpGraduated.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFamily3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFamily2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFamily1)).BeginInit();
            this.grpFamily.SuspendLayout();
            this.grpExperience.SuspendLayout();
            this.grpCity.SuspendLayout();
            this.grpLanguage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOtherLanguages)).BeginInit();
            this.SuspendLayout();
            // 
            // chkLanguage1
            // 
            this.chkLanguage1.AutoSize = true;
            this.chkLanguage1.Location = new System.Drawing.Point(6, 18);
            this.chkLanguage1.Name = "chkLanguage1";
            this.chkLanguage1.Size = new System.Drawing.Size(132, 21);
            this.chkLanguage1.TabIndex = 0;
            this.chkLanguage1.Tag = "0.20";
            this.chkLanguage1.Text = "Certified English";
            this.chkLanguage1.UseVisualStyleBackColor = true;
            // 
            // chkFamily1
            // 
            this.chkFamily1.AutoSize = true;
            this.chkFamily1.Location = new System.Drawing.Point(6, 18);
            this.chkFamily1.Name = "chkFamily1";
            this.chkFamily1.Size = new System.Drawing.Size(193, 21);
            this.chkFamily1.TabIndex = 0;
            this.chkFamily1.Tag = "0.20";
            this.chkFamily1.Text = "Married-Wife Not Working";
            this.chkFamily1.UseVisualStyleBackColor = true;
            // 
            // grpTask
            // 
            this.grpTask.Controls.Add(this.rbTask6);
            this.grpTask.Controls.Add(this.rbTask5);
            this.grpTask.Controls.Add(this.rbTask4);
            this.grpTask.Controls.Add(this.rbTask3);
            this.grpTask.Controls.Add(this.rbTask2);
            this.grpTask.Controls.Add(this.rbTask1);
            this.grpTask.Location = new System.Drawing.Point(48, 12);
            this.grpTask.Name = "grpTask";
            this.grpTask.Size = new System.Drawing.Size(409, 189);
            this.grpTask.TabIndex = 11;
            this.grpTask.TabStop = false;
            this.grpTask.Text = "Task";
            this.grpTask.Visible = false;
            // 
            // rbTask6
            // 
            this.rbTask6.AutoSize = true;
            this.rbTask6.Location = new System.Drawing.Point(6, 157);
            this.rbTask6.Name = "rbTask6";
            this.rbTask6.Size = new System.Drawing.Size(195, 21);
            this.rbTask6.TabIndex = 5;
            this.rbTask6.TabStop = true;
            this.rbTask6.Tag = "0.60";
            this.rbTask6.Text = "IT Manager ( +5 Personal)";
            this.rbTask6.UseVisualStyleBackColor = true;
            // 
            // rbTask5
            // 
            this.rbTask5.AutoSize = true;
            this.rbTask5.Location = new System.Drawing.Point(6, 130);
            this.rbTask5.Name = "rbTask5";
            this.rbTask5.Size = new System.Drawing.Size(101, 21);
            this.rbTask5.TabIndex = 4;
            this.rbTask5.TabStop = true;
            this.rbTask5.Tag = "0.40";
            this.rbTask5.Text = "IT Manager";
            this.rbTask5.UseVisualStyleBackColor = true;
            // 
            // rbTask4
            // 
            this.rbTask4.AutoSize = true;
            this.rbTask4.Location = new System.Drawing.Point(6, 103);
            this.rbTask4.Name = "rbTask4";
            this.rbTask4.Size = new System.Drawing.Size(58, 21);
            this.rbTask4.TabIndex = 3;
            this.rbTask4.TabStop = true;
            this.rbTask4.Tag = "1.00";
            this.rbTask4.Text = "CTO";
            this.rbTask4.UseVisualStyleBackColor = true;
            // 
            // rbTask3
            // 
            this.rbTask3.AutoSize = true;
            this.rbTask3.Location = new System.Drawing.Point(6, 76);
            this.rbTask3.Name = "rbTask3";
            this.rbTask3.Size = new System.Drawing.Size(79, 21);
            this.rbTask3.TabIndex = 2;
            this.rbTask3.TabStop = true;
            this.rbTask3.Tag = "0.85";
            this.rbTask3.Text = "Director";
            this.rbTask3.UseVisualStyleBackColor = true;
            // 
            // rbTask2
            // 
            this.rbTask2.AutoSize = true;
            this.rbTask2.Location = new System.Drawing.Point(6, 49);
            this.rbTask2.Name = "rbTask2";
            this.rbTask2.Size = new System.Drawing.Size(109, 21);
            this.rbTask2.TabIndex = 1;
            this.rbTask2.TabStop = true;
            this.rbTask2.Tag = "0.75";
            this.rbTask2.Text = "Project Lead";
            this.rbTask2.UseVisualStyleBackColor = true;
            // 
            // rbTask1
            // 
            this.rbTask1.AutoSize = true;
            this.rbTask1.Location = new System.Drawing.Point(6, 21);
            this.rbTask1.Name = "rbTask1";
            this.rbTask1.Size = new System.Drawing.Size(145, 21);
            this.rbTask1.TabIndex = 0;
            this.rbTask1.TabStop = true;
            this.rbTask1.Tag = "0.50";
            this.rbTask1.Text = "Team/Group Lead";
            this.rbTask1.UseVisualStyleBackColor = true;
            // 
            // grpGraduated
            // 
            this.grpGraduated.Controls.Add(this.chkGraduated5);
            this.grpGraduated.Controls.Add(this.chkGraduated3);
            this.grpGraduated.Controls.Add(this.chkGraduated4);
            this.grpGraduated.Controls.Add(this.chkGraduated1);
            this.grpGraduated.Controls.Add(this.chkGraduated2);
            this.grpGraduated.Location = new System.Drawing.Point(48, 12);
            this.grpGraduated.Name = "grpGraduated";
            this.grpGraduated.Size = new System.Drawing.Size(409, 189);
            this.grpGraduated.TabIndex = 10;
            this.grpGraduated.TabStop = false;
            this.grpGraduated.Text = "Graduated";
            this.grpGraduated.Visible = false;
            // 
            // chkGraduated5
            // 
            this.chkGraduated5.AutoSize = true;
            this.chkGraduated5.Location = new System.Drawing.Point(6, 128);
            this.chkGraduated5.Name = "chkGraduated5";
            this.chkGraduated5.Size = new System.Drawing.Size(230, 21);
            this.chkGraduated5.TabIndex = 4;
            this.chkGraduated5.Tag = "0.15";
            this.chkGraduated5.Text = "Doctorate/Docent (Not Related)";
            this.chkGraduated5.UseVisualStyleBackColor = true;
            // 
            // chkGraduated3
            // 
            this.chkGraduated3.AutoSize = true;
            this.chkGraduated3.Location = new System.Drawing.Point(6, 74);
            this.chkGraduated3.Name = "chkGraduated3";
            this.chkGraduated3.Size = new System.Drawing.Size(75, 21);
            this.chkGraduated3.TabIndex = 2;
            this.chkGraduated3.Tag = "0.35";
            this.chkGraduated3.Text = "Docent";
            this.chkGraduated3.UseVisualStyleBackColor = true;
            // 
            // chkGraduated4
            // 
            this.chkGraduated4.AutoSize = true;
            this.chkGraduated4.Location = new System.Drawing.Point(6, 101);
            this.chkGraduated4.Name = "chkGraduated4";
            this.chkGraduated4.Size = new System.Drawing.Size(162, 21);
            this.chkGraduated4.TabIndex = 3;
            this.chkGraduated4.Tag = "0.05";
            this.chkGraduated4.Text = "Master (Not Related)";
            this.chkGraduated4.UseVisualStyleBackColor = true;
            // 
            // chkGraduated1
            // 
            this.chkGraduated1.AutoSize = true;
            this.chkGraduated1.Location = new System.Drawing.Point(6, 21);
            this.chkGraduated1.Name = "chkGraduated1";
            this.chkGraduated1.Size = new System.Drawing.Size(73, 21);
            this.chkGraduated1.TabIndex = 0;
            this.chkGraduated1.Tag = "0.10";
            this.chkGraduated1.Text = "Master";
            this.chkGraduated1.UseVisualStyleBackColor = true;
            // 
            // chkGraduated2
            // 
            this.chkGraduated2.AutoSize = true;
            this.chkGraduated2.Location = new System.Drawing.Point(6, 48);
            this.chkGraduated2.Name = "chkGraduated2";
            this.chkGraduated2.Size = new System.Drawing.Size(92, 21);
            this.chkGraduated2.TabIndex = 1;
            this.chkGraduated2.Tag = "0.30";
            this.chkGraduated2.Text = "Doctorate";
            this.chkGraduated2.UseVisualStyleBackColor = true;
            // 
            // chkFamily4
            // 
            this.chkFamily4.AutoSize = true;
            this.chkFamily4.Location = new System.Drawing.Point(6, 99);
            this.chkFamily4.Name = "chkFamily4";
            this.chkFamily4.Size = new System.Drawing.Size(99, 21);
            this.chkFamily4.TabIndex = 5;
            this.chkFamily4.Tag = "0.40";
            this.chkFamily4.Text = "Child (+18)";
            this.chkFamily4.UseVisualStyleBackColor = true;
            // 
            // chkFamily3
            // 
            this.chkFamily3.AutoSize = true;
            this.chkFamily3.Location = new System.Drawing.Point(6, 72);
            this.chkFamily3.Name = "chkFamily3";
            this.chkFamily3.Size = new System.Drawing.Size(104, 21);
            this.chkFamily3.TabIndex = 4;
            this.chkFamily3.Tag = "0.30";
            this.chkFamily3.Text = "Child (7-18)";
            this.chkFamily3.UseVisualStyleBackColor = true;
            // 
            // numFamily3
            // 
            this.numFamily3.Location = new System.Drawing.Point(132, 98);
            this.numFamily3.Name = "numFamily3";
            this.numFamily3.Size = new System.Drawing.Size(64, 22);
            this.numFamily3.TabIndex = 3;
            this.numFamily3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numFamily2
            // 
            this.numFamily2.Location = new System.Drawing.Point(132, 70);
            this.numFamily2.Name = "numFamily2";
            this.numFamily2.Size = new System.Drawing.Size(64, 22);
            this.numFamily2.TabIndex = 3;
            this.numFamily2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numFamily1
            // 
            this.numFamily1.Location = new System.Drawing.Point(132, 44);
            this.numFamily1.Name = "numFamily1";
            this.numFamily1.Size = new System.Drawing.Size(64, 22);
            this.numFamily1.TabIndex = 3;
            this.numFamily1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chkFamily2
            // 
            this.chkFamily2.AutoSize = true;
            this.chkFamily2.Location = new System.Drawing.Point(6, 45);
            this.chkFamily2.Name = "chkFamily2";
            this.chkFamily2.Size = new System.Drawing.Size(96, 21);
            this.chkFamily2.TabIndex = 1;
            this.chkFamily2.Tag = "0.20";
            this.chkFamily2.Text = "Child (0-6)";
            this.chkFamily2.UseVisualStyleBackColor = true;
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnNext.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnNext.Location = new System.Drawing.Point(199, 258);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(117, 45);
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // grpFamily
            // 
            this.grpFamily.Controls.Add(this.chkFamily4);
            this.grpFamily.Controls.Add(this.chkFamily3);
            this.grpFamily.Controls.Add(this.numFamily3);
            this.grpFamily.Controls.Add(this.numFamily2);
            this.grpFamily.Controls.Add(this.numFamily1);
            this.grpFamily.Controls.Add(this.chkFamily2);
            this.grpFamily.Controls.Add(this.chkFamily1);
            this.grpFamily.Location = new System.Drawing.Point(48, 12);
            this.grpFamily.Name = "grpFamily";
            this.grpFamily.Size = new System.Drawing.Size(409, 189);
            this.grpFamily.TabIndex = 12;
            this.grpFamily.TabStop = false;
            this.grpFamily.Text = "Family";
            this.grpFamily.Visible = false;
            // 
            // grpExperience
            // 
            this.grpExperience.Controls.Add(this.rbExperience6);
            this.grpExperience.Controls.Add(this.rbExperience5);
            this.grpExperience.Controls.Add(this.rbExperience4);
            this.grpExperience.Controls.Add(this.rbExperience3);
            this.grpExperience.Controls.Add(this.rbExperience2);
            this.grpExperience.Controls.Add(this.rbExperience1);
            this.grpExperience.Location = new System.Drawing.Point(48, 12);
            this.grpExperience.Name = "grpExperience";
            this.grpExperience.Size = new System.Drawing.Size(409, 189);
            this.grpExperience.TabIndex = 7;
            this.grpExperience.TabStop = false;
            this.grpExperience.Text = "Experience";
            // 
            // rbExperience6
            // 
            this.rbExperience6.AutoSize = true;
            this.rbExperience6.Location = new System.Drawing.Point(6, 157);
            this.rbExperience6.Name = "rbExperience6";
            this.rbExperience6.Size = new System.Drawing.Size(94, 21);
            this.rbExperience6.TabIndex = 5;
            this.rbExperience6.TabStop = true;
            this.rbExperience6.Tag = "1.50";
            this.rbExperience6.Text = "20+ Years";
            this.rbExperience6.UseVisualStyleBackColor = true;
            // 
            // rbExperience5
            // 
            this.rbExperience5.AutoSize = true;
            this.rbExperience5.Location = new System.Drawing.Point(6, 130);
            this.rbExperience5.Name = "rbExperience5";
            this.rbExperience5.Size = new System.Drawing.Size(107, 21);
            this.rbExperience5.TabIndex = 4;
            this.rbExperience5.TabStop = true;
            this.rbExperience5.Tag = "1.35";
            this.rbExperience5.Text = "15-19 Years";
            this.rbExperience5.UseVisualStyleBackColor = true;
            // 
            // rbExperience4
            // 
            this.rbExperience4.AutoSize = true;
            this.rbExperience4.Location = new System.Drawing.Point(6, 103);
            this.rbExperience4.Name = "rbExperience4";
            this.rbExperience4.Size = new System.Drawing.Size(107, 21);
            this.rbExperience4.TabIndex = 3;
            this.rbExperience4.TabStop = true;
            this.rbExperience4.Tag = "1.20";
            this.rbExperience4.Text = "10-14 Years";
            this.rbExperience4.UseVisualStyleBackColor = true;
            // 
            // rbExperience3
            // 
            this.rbExperience3.AutoSize = true;
            this.rbExperience3.Location = new System.Drawing.Point(6, 76);
            this.rbExperience3.Name = "rbExperience3";
            this.rbExperience3.Size = new System.Drawing.Size(91, 21);
            this.rbExperience3.TabIndex = 2;
            this.rbExperience3.TabStop = true;
            this.rbExperience3.Tag = "1.00";
            this.rbExperience3.Text = "5-9 Years";
            this.rbExperience3.UseVisualStyleBackColor = true;
            // 
            // rbExperience2
            // 
            this.rbExperience2.AutoSize = true;
            this.rbExperience2.Location = new System.Drawing.Point(6, 49);
            this.rbExperience2.Name = "rbExperience2";
            this.rbExperience2.Size = new System.Drawing.Size(91, 21);
            this.rbExperience2.TabIndex = 1;
            this.rbExperience2.TabStop = true;
            this.rbExperience2.Tag = "0.60";
            this.rbExperience2.Text = "2-4 Years";
            this.rbExperience2.UseVisualStyleBackColor = true;
            // 
            // rbExperience1
            // 
            this.rbExperience1.AutoSize = true;
            this.rbExperience1.Location = new System.Drawing.Point(6, 21);
            this.rbExperience1.Name = "rbExperience1";
            this.rbExperience1.Size = new System.Drawing.Size(91, 21);
            this.rbExperience1.TabIndex = 0;
            this.rbExperience1.TabStop = true;
            this.rbExperience1.Tag = "0.0";
            this.rbExperience1.Text = "0-2 Years";
            this.rbExperience1.UseVisualStyleBackColor = true;
            // 
            // grpCity
            // 
            this.grpCity.Controls.Add(this.rbCity1);
            this.grpCity.Controls.Add(this.rbCity2);
            this.grpCity.Controls.Add(this.rbCity3);
            this.grpCity.Controls.Add(this.rbCity4);
            this.grpCity.Location = new System.Drawing.Point(48, 12);
            this.grpCity.Name = "grpCity";
            this.grpCity.Size = new System.Drawing.Size(409, 189);
            this.grpCity.TabIndex = 8;
            this.grpCity.TabStop = false;
            this.grpCity.Text = "City";
            this.grpCity.Visible = false;
            // 
            // rbCity1
            // 
            this.rbCity1.AutoSize = true;
            this.rbCity1.Location = new System.Drawing.Point(6, 21);
            this.rbCity1.Name = "rbCity1";
            this.rbCity1.Size = new System.Drawing.Size(78, 21);
            this.rbCity1.TabIndex = 0;
            this.rbCity1.TabStop = true;
            this.rbCity1.Tag = "0.15";
            this.rbCity1.Text = "Istanbul";
            this.rbCity1.UseVisualStyleBackColor = true;
            // 
            // rbCity2
            // 
            this.rbCity2.AutoSize = true;
            this.rbCity2.Location = new System.Drawing.Point(6, 48);
            this.rbCity2.Name = "rbCity2";
            this.rbCity2.Size = new System.Drawing.Size(108, 21);
            this.rbCity2.TabIndex = 1;
            this.rbCity2.TabStop = true;
            this.rbCity2.Tag = "0.10";
            this.rbCity2.Text = "Ankara-Izmir";
            this.rbCity2.UseVisualStyleBackColor = true;
            // 
            // rbCity3
            // 
            this.rbCity3.AutoSize = true;
            this.rbCity3.Location = new System.Drawing.Point(6, 75);
            this.rbCity3.Name = "rbCity3";
            this.rbCity3.Size = new System.Drawing.Size(104, 21);
            this.rbCity3.TabIndex = 2;
            this.rbCity3.TabStop = true;
            this.rbCity3.Tag = "0.05";
            this.rbCity3.Text = "TR42-TR21";
            this.rbCity3.UseVisualStyleBackColor = true;
            // 
            // rbCity4
            // 
            this.rbCity4.AutoSize = true;
            this.rbCity4.Location = new System.Drawing.Point(6, 102);
            this.rbCity4.Name = "rbCity4";
            this.rbCity4.Size = new System.Drawing.Size(72, 21);
            this.rbCity4.TabIndex = 3;
            this.rbCity4.TabStop = true;
            this.rbCity4.Tag = "0.03";
            this.rbCity4.Text = "Others";
            this.rbCity4.UseVisualStyleBackColor = true;
            // 
            // chkLanguage2
            // 
            this.chkLanguage2.AutoSize = true;
            this.chkLanguage2.Location = new System.Drawing.Point(6, 45);
            this.chkLanguage2.Name = "chkLanguage2";
            this.chkLanguage2.Size = new System.Drawing.Size(173, 21);
            this.chkLanguage2.TabIndex = 1;
            this.chkLanguage2.Tag = "0.20";
            this.chkLanguage2.Text = "School (%100 English)";
            this.chkLanguage2.UseVisualStyleBackColor = true;
            // 
            // grpLanguage
            // 
            this.grpLanguage.Controls.Add(this.numOtherLanguages);
            this.grpLanguage.Controls.Add(this.chkLanguage3);
            this.grpLanguage.Controls.Add(this.chkLanguage2);
            this.grpLanguage.Controls.Add(this.chkLanguage1);
            this.grpLanguage.Location = new System.Drawing.Point(48, 12);
            this.grpLanguage.Name = "grpLanguage";
            this.grpLanguage.Size = new System.Drawing.Size(409, 189);
            this.grpLanguage.TabIndex = 9;
            this.grpLanguage.TabStop = false;
            this.grpLanguage.Text = "Language";
            this.grpLanguage.Visible = false;
            // 
            // numOtherLanguages
            // 
            this.numOtherLanguages.Location = new System.Drawing.Point(153, 71);
            this.numOtherLanguages.Name = "numOtherLanguages";
            this.numOtherLanguages.Size = new System.Drawing.Size(64, 22);
            this.numOtherLanguages.TabIndex = 3;
            this.numOtherLanguages.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chkLanguage3
            // 
            this.chkLanguage3.AutoSize = true;
            this.chkLanguage3.Location = new System.Drawing.Point(6, 72);
            this.chkLanguage3.Name = "chkLanguage3";
            this.chkLanguage3.Size = new System.Drawing.Size(141, 21);
            this.chkLanguage3.TabIndex = 1;
            this.chkLanguage3.Tag = "0.05";
            this.chkLanguage3.Text = "Other Languages";
            this.chkLanguage3.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCancel.Location = new System.Drawing.Point(199, 309);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(117, 45);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(13, 221);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(0, 17);
            this.lblInfo.TabIndex = 14;
            // 
            // Form_Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 355);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.grpTask);
            this.Controls.Add(this.grpGraduated);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.grpFamily);
            this.Controls.Add(this.grpExperience);
            this.Controls.Add(this.grpCity);
            this.Controls.Add(this.grpLanguage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form_Calculator";
            this.Text = "SALARY CALCULATOR";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Calculator_FormClosing);
            this.grpTask.ResumeLayout(false);
            this.grpTask.PerformLayout();
            this.grpGraduated.ResumeLayout(false);
            this.grpGraduated.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFamily3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFamily2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFamily1)).EndInit();
            this.grpFamily.ResumeLayout(false);
            this.grpFamily.PerformLayout();
            this.grpExperience.ResumeLayout(false);
            this.grpExperience.PerformLayout();
            this.grpCity.ResumeLayout(false);
            this.grpCity.PerformLayout();
            this.grpLanguage.ResumeLayout(false);
            this.grpLanguage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOtherLanguages)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkLanguage1;
        private System.Windows.Forms.CheckBox chkFamily1;
        private System.Windows.Forms.GroupBox grpTask;
        private System.Windows.Forms.RadioButton rbTask6;
        private System.Windows.Forms.RadioButton rbTask5;
        private System.Windows.Forms.RadioButton rbTask4;
        private System.Windows.Forms.RadioButton rbTask3;
        private System.Windows.Forms.RadioButton rbTask2;
        private System.Windows.Forms.RadioButton rbTask1;
        private System.Windows.Forms.GroupBox grpGraduated;
        private System.Windows.Forms.CheckBox chkGraduated5;
        private System.Windows.Forms.CheckBox chkGraduated3;
        private System.Windows.Forms.CheckBox chkGraduated4;
        private System.Windows.Forms.CheckBox chkGraduated1;
        private System.Windows.Forms.CheckBox chkGraduated2;
        private System.Windows.Forms.CheckBox chkFamily4;
        private System.Windows.Forms.CheckBox chkFamily3;
        private System.Windows.Forms.NumericUpDown numFamily3;
        private System.Windows.Forms.NumericUpDown numFamily2;
        private System.Windows.Forms.NumericUpDown numFamily1;
        private System.Windows.Forms.CheckBox chkFamily2;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.GroupBox grpFamily;
        private System.Windows.Forms.GroupBox grpExperience;
        private System.Windows.Forms.RadioButton rbExperience6;
        private System.Windows.Forms.RadioButton rbExperience5;
        private System.Windows.Forms.RadioButton rbExperience4;
        private System.Windows.Forms.RadioButton rbExperience3;
        private System.Windows.Forms.RadioButton rbExperience2;
        private System.Windows.Forms.RadioButton rbExperience1;
        private System.Windows.Forms.GroupBox grpCity;
        private System.Windows.Forms.RadioButton rbCity1;
        private System.Windows.Forms.RadioButton rbCity2;
        private System.Windows.Forms.RadioButton rbCity3;
        private System.Windows.Forms.RadioButton rbCity4;
        private System.Windows.Forms.CheckBox chkLanguage2;
        private System.Windows.Forms.GroupBox grpLanguage;
        private System.Windows.Forms.NumericUpDown numOtherLanguages;
        private System.Windows.Forms.CheckBox chkLanguage3;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblInfo;
    }
}