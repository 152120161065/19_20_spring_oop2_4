﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_Phonebook : Form
    {
        public Form_Phonebook()
        {
            InitializeComponent();
        }
        private void Form_Phonebook_Load(object sender, EventArgs e)
        {
            //Set data source for data grid view
            dgvPhonebook.DataSource = ActiveUser.getInstance().Contactlist;
            //Is there any contact? --> If so, we need the update and delete button!
            if (!dgvPhonebook.Rows.Count.Equals(0))
            {
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
            }
        }
        private void btnCreate_Click(object sender, EventArgs e)
        {
            //Load Create Form
            Form create = new Form_CreateContact();
            create.Show();
            this.Hide();
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //Set selected contact
            ActiveUser.getInstance().Selectedcontact = dgvPhonebook.CurrentRow.DataBoundItem as Contact;
            //Load Update Form
            Form update = new Form_UpdateContact();
            update.Show();
            this.Hide();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure?", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                //Remove from list and file
                Contact selectedcontact = dgvPhonebook.CurrentRow.DataBoundItem as Contact;
                FileOperations.getInstance().DeleteMethod(selectedcontact.ToString(), @"path\phonebook.csv");
                ActiveUser.getInstance().Contactlist.Remove(selectedcontact);
            }
            //After delete process, is phonebook empty? ---> If so we don't need these buttons anymore!
            if (dgvPhonebook.Rows.Count.Equals(0))
            {
                btnDelete.Enabled = false;
                btnUpdate.Enabled = false;
            }
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            //Go back menu
            Form menu = new Form_Menu();
            menu.Show();
            this.Hide();
        }
        private void Form_Phonebook_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
