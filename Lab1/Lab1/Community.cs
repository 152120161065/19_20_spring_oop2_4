﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lab1
{
    class Community
    {
        //Definitions of properties
        private BindingList<User> userlist { get; set; }
        Regex MailFormat;
        //Getter-Setter methods
        public BindingList<User> Userlist { get => userlist; set => userlist = value; }
        /// <summary>
        /// Check availability create new user according to given parameters
        /// </summary>
        /// <param name="username"> User specified username </param>
        /// <returns> Return availability </returns>
        public bool Check_CreateUser(string username,string email,string number)
        {
            if(MailFormat.IsMatch(email))
            {
                foreach (User user in userlist)
                {
                    if (user.Username.Equals(username) || user.Email.Equals(email) || user.Number.Equals(number))
                        return false;
                }
                return true;
            }
            return false;
        }
        /// <summary>
        /// Search in status = 1, if so find it.
        /// </summary>
        /// <returns> Return search result </returns>
        public bool Check_RememberMe()
        {
            foreach (User user in userlist)
            {
                if (user.Status == 1)
                {
                    ActiveUser.getInstance().User = user;
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Check login validation according to given parameters
        /// </summary>
        /// <param name="username"> User specified username </param>
        /// <param name="password"> User specified password </param>
        /// <returns> Return validation result </returns>
        public bool Check_Login(string username, string password)
        {
            password = Crypt.getInstance().Convert_Sha256(password);
            foreach (User user in userlist)
            {
                if (user.Username.Equals(username) && user.Password.Equals(password))
                {
                    ActiveUser.getInstance().User = user;
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Singleton design pattern
        /// </summary>
        private static Community instance;
        private Community()
        {
            MailFormat = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
            userlist = new BindingList<User>();
        }
        public static Community getInstance()
        {
            if (instance == null)
            {
                instance = new Community();
            }
            return instance;
        }
    }
}
