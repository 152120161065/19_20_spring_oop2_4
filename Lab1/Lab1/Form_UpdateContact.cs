﻿//152120161064_FERSU_EKINCI
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_UpdateContact : Form
    {
        Contact selectedContact = ActiveUser.getInstance().Selectedcontact;
        public Form_UpdateContact()
        {
            InitializeComponent();
        }
        private void Form_UpdateContact_Load(object sender, EventArgs e)
        {
            //Set txtboxes with contact infos
            txtName.Text = selectedContact.Name;
            txtSurname.Text = selectedContact.Surname;
            txtNumber.Text = selectedContact.Number;
            txtEmail.Text = selectedContact.Email;
            txtDescription.Text = selectedContact.Description;
            txtAddress.Text = selectedContact.Address;
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            if (this.Controls.OfType<TextBox>().Any(t => string.IsNullOrEmpty(t.Text)))
            {
                lblInfo.Text = "Fill empty fields";
                lblInfo.ForeColor = Color.DarkRed;
                return;
            }
            if (ActiveUser.getInstance().Check_UpdateContact(txtEmail.Text, txtNumber.Text))
            {
                Update_Success();
                return;
            }
            Update_Failed();
        }
        private void Update_Success()
        {
            //Set oldValue for update method
            string oldValue = selectedContact.ToString();
            //Apply new properties to contact
            selectedContact.Name = txtName.Text;
            selectedContact.Surname = txtSurname.Text;
            selectedContact.Number = txtNumber.Text;
            selectedContact.Email = txtEmail.Text;
            selectedContact.Description = txtEmail.Text;
            selectedContact.Address = txtAddress.Text;
            //File process
            FileOperations.getInstance().UpdateMethod(oldValue, selectedContact.ToString(), @"path\phonebook.csv");
            //Go back phonebook
            Form phonebook = new Form_Phonebook();
            phonebook.Show();
            this.Hide();
        }
        private void Update_Failed()
        {
            //Failed
            lblInfo.Text = "Invalid email or contact already exist";
            lblInfo.ForeColor = Color.DarkRed;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            //Go back phonebook
            Form phonebook = new Form_Phonebook();
            phonebook.Show();
            this.Hide();
        }
        private void Form_UpdateContact_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
