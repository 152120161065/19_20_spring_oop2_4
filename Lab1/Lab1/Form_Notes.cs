﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form_Notes : Form
    {
        public Form_Notes()
        {
            InitializeComponent();
        }
        private void Form_Notes_Load(object sender, EventArgs e)
        {
            //Set data source for data grid view
            dgvNotes.DataSource = ActiveUser.getInstance().Notelist;
            //Do we need delete and update button?
            if (dgvNotes.Rows.Count != 0)
            {
                btnDelete.Enabled = true;
                btnUpdate.Enabled = true;
            }
        }
        private void btnCreate_Click(object sender, EventArgs e)
        {
            //Load create form
            Form create = new Form_CreateNote();
            create.Show();
            this.Hide();
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //Set note for update
            ActiveUser.getInstance().Selectednote = dgvNotes.CurrentRow.DataBoundItem as Notes;
            //Load update form
            Form update = new Form_UpdateNote();
            update.Show();
            this.Hide();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure?", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Notes note = dgvNotes.CurrentRow.DataBoundItem as Notes;
                //Delete from list and file
                FileOperations.getInstance().DeleteMethod(note.ToString(), @"path\notes.csv");
                ActiveUser.getInstance().Notelist.Remove(note);
            }
            //Don't have note? We don't need these buttons
            if (dgvNotes.Rows.Count == 0)
            {
                btnDelete.Enabled = false;
                btnUpdate.Enabled = false;
            }
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            //Back to Menu
            Form menu = new Form_Menu();
            menu.Show();
            this.Hide();
        }
        private void Form_Notes_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
