﻿namespace Lab1
{
    partial class Form_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExport = new System.Windows.Forms.Button();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.sfdExport = new System.Windows.Forms.SaveFileDialog();
            this.btnManage = new System.Windows.Forms.Button();
            this.btnPhonebook = new System.Windows.Forms.Button();
            this.btnNotes = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnReminder = new System.Windows.Forms.Button();
            this.btnCalculator = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnExport.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnExport.Location = new System.Drawing.Point(214, 224);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(172, 83);
            this.btnExport.TabIndex = 6;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnLogOut
            // 
            this.btnLogOut.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnLogOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnLogOut.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnLogOut.Location = new System.Drawing.Point(214, 327);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(172, 83);
            this.btnLogOut.TabIndex = 7;
            this.btnLogOut.Text = "Log Out";
            this.btnLogOut.UseVisualStyleBackColor = false;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // sfdExport
            // 
            this.sfdExport.DefaultExt = "tsv";
            this.sfdExport.Filter = "tsvFile | *.tsv";
            // 
            // btnManage
            // 
            this.btnManage.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnManage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnManage.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnManage.Location = new System.Drawing.Point(214, 116);
            this.btnManage.Name = "btnManage";
            this.btnManage.Size = new System.Drawing.Size(172, 83);
            this.btnManage.TabIndex = 5;
            this.btnManage.Text = "User Management";
            this.btnManage.UseVisualStyleBackColor = false;
            this.btnManage.Click += new System.EventHandler(this.btnManage_Click);
            // 
            // btnPhonebook
            // 
            this.btnPhonebook.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnPhonebook.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnPhonebook.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPhonebook.Location = new System.Drawing.Point(12, 12);
            this.btnPhonebook.Name = "btnPhonebook";
            this.btnPhonebook.Size = new System.Drawing.Size(172, 83);
            this.btnPhonebook.TabIndex = 0;
            this.btnPhonebook.Text = "Phonebook";
            this.btnPhonebook.UseVisualStyleBackColor = false;
            this.btnPhonebook.Click += new System.EventHandler(this.btnPhonebook_Click);
            // 
            // btnNotes
            // 
            this.btnNotes.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnNotes.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnNotes.Location = new System.Drawing.Point(12, 116);
            this.btnNotes.Name = "btnNotes";
            this.btnNotes.Size = new System.Drawing.Size(172, 83);
            this.btnNotes.TabIndex = 1;
            this.btnNotes.Text = "Notes";
            this.btnNotes.UseVisualStyleBackColor = false;
            this.btnNotes.Click += new System.EventHandler(this.btnNotes_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnEdit.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnEdit.Location = new System.Drawing.Point(214, 12);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(172, 83);
            this.btnEdit.TabIndex = 4;
            this.btnEdit.Text = "Edit Profile";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnReminder
            // 
            this.btnReminder.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnReminder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnReminder.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnReminder.Location = new System.Drawing.Point(12, 224);
            this.btnReminder.Name = "btnReminder";
            this.btnReminder.Size = new System.Drawing.Size(172, 83);
            this.btnReminder.TabIndex = 2;
            this.btnReminder.Text = "Reminder";
            this.btnReminder.UseVisualStyleBackColor = false;
            this.btnReminder.Click += new System.EventHandler(this.btnReminder_Click);
            // 
            // btnCalculator
            // 
            this.btnCalculator.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnCalculator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnCalculator.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCalculator.Location = new System.Drawing.Point(12, 327);
            this.btnCalculator.Name = "btnCalculator";
            this.btnCalculator.Size = new System.Drawing.Size(172, 83);
            this.btnCalculator.TabIndex = 3;
            this.btnCalculator.Text = "Calculator";
            this.btnCalculator.UseVisualStyleBackColor = false;
            this.btnCalculator.Click += new System.EventHandler(this.btnCalculator_Click);
            // 
            // Form_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 422);
            this.Controls.Add(this.btnCalculator);
            this.Controls.Add(this.btnReminder);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnNotes);
            this.Controls.Add(this.btnPhonebook);
            this.Controls.Add(this.btnManage);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.btnExport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form_Menu";
            this.Text = "MENU";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_App_FormClosing);
            this.Load += new System.EventHandler(this.Form_App_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.SaveFileDialog sfdExport;
        private System.Windows.Forms.Button btnManage;
        private System.Windows.Forms.Button btnPhonebook;
        private System.Windows.Forms.Button btnNotes;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnReminder;
        private System.Windows.Forms.Button btnCalculator;
    }
}